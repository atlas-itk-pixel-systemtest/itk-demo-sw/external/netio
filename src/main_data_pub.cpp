#include <iostream>
#include <chrono>
#include <unistd.h>

#include "netio/netio.hpp"


const char* HELP =
    R"FOO(Usage: netio_data_pub [options]

Publishes messages of a given size under a given set of tags.

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port on the remote. Default: 12345.
  -m BYTES        The size of a message that is transferred in bytes. Default: 1024.
  -n NUM          The number of tags to use. The tags {0, ..., NUM-1} are unsigned
                  in a round-robin fashion. Default: 1.
  -k KHZ          Limit publish rate to the specified frequency in kHz.
  -h              Display this help message.
)FOO";


typedef std::uint64_t timestamp_t;

static const timestamp_t ns = 1;
static const timestamp_t us = 1000 * ns;
static const timestamp_t ms = 1000 * us;
static const timestamp_t s = 1000 * ms;

timestamp_t gettime()
{
	::timespec ts;
	::clock_gettime(CLOCK_MONOTONIC, &ts);
	return timestamp_t(ts.tv_sec)*s + timestamp_t(ts.tv_nsec)*ns;
}



static void
publish(netio::context* ctx,
		const char* host,
        unsigned short port,
        unsigned num_tags,
        size_t msgsize
        )
{
	netio::publish_socket socket(ctx, port);

	char* data = new char[msgsize];
	for(unsigned int i=0; i<msgsize; i++)
	{
		data[i] = 'x';
	}
	netio::message msg((uint8_t*)data, msgsize);

	int tag = 0;
	while(true)
	{
		socket.publish(tag, msg);
		tag = (tag + 1) % num_tags;
    }
}


static void
publish_with_rate_limitation(netio::context* ctx,
			    const char* host,
			    unsigned short port,
			    unsigned num_tags,
			    size_t msgsize,
			    unsigned long long kilohertz
			    )
{
  netio::publish_socket socket(ctx, port);

  timestamp_t max_overshoot = 10 * ms;
  timestamp_t period = (1000./kilohertz) * us;

  timestamp_t now = gettime();
  timestamp_t deadline = now+period;


  char* data = new char[msgsize];
  for(unsigned int i=0; i<msgsize; i++)
    {
      data[i] = 'x';
    }
  netio::message msg((uint8_t*)data, msgsize);

  int tag = 0;
  while(true)
    {
      socket.publish(tag, msg);
      tag = (tag + 1) % num_tags;

      if(now > deadline + max_overshoot) {
	deadline = now+period;
      } else {
	while(now < deadline) {
	  now = gettime();
	}
	deadline += period;
      }
    }
}


int
main(int argc, char** argv)
{
	unsigned short port = 12345;
	std::string host = "127.0.0.1";
	std::string backend = "posix";
	size_t msgsize = 1024;
	unsigned num_tags = 1;
	unsigned long long kilohertz = 0;

	char opt;
	while ((opt = getopt(argc, argv, "b:H:p:m:n:k:h")) != -1)
	{
		switch (opt)
		{
		case 'b':
			backend = optarg;
			break;
		case 'H':
			host = optarg;
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'm':
			msgsize = atoi(optarg);
			break;
		case 'n':
			num_tags = atoi(optarg);
			break;
		case 'k':
		  kilohertz = atoi(optarg);
		  break;
		case 'h':
		default:
			std::cerr << HELP << std::endl;
			return -1;
		}
	}

	netio::context ctx(backend.c_str());
	std::thread bg_thread([&ctx](){
		ctx.event_loop()->run_forever();
	});

	if(kilohertz > 0) {
          std::cout << "running with rate limited to " << kilohertz << " kHz" << std::endl;
	  publish_with_rate_limitation(&ctx, host.c_str(), port, num_tags, msgsize, kilohertz);
	} else {
	  publish(&ctx, host.c_str(), port, num_tags, msgsize);
	}

	ctx.event_loop()->stop();
	bg_thread.join();
	return 0;
}
