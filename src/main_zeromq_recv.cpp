#include "zmq.hpp"
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <sstream>
#include <unistd.h>



const char* HELP =
    R"FOO(Usage: netio_zeromq_recv [options]

Listens on a port and receives incoming 0MQ messages.

Options are:

  -p PORT         Listen on the specified port on the remote. Default: 12345.
  -h              Display this help message.
)FOO";


int
main(int argc, char** argv)
{
    unsigned short port = 12345;

    char opt;
    while ((opt = getopt(argc, argv, "p:h")) != -1)
    {
        switch (opt)
        {
        case 'p':
            port = atoi(optarg);
            break;
        case 'h':
        default: 
            std::cerr << HELP << std::endl;
            return -1;
        }
    }


    //  Prepare our context and socket
    zmq::context_t context(1);
    zmq::socket_t receiver(context, ZMQ_PULL);
    std::stringstream s;
    s << "tcp://*:" << port;
    receiver.bind(s.str().c_str());

    zmq::message_t message;
    int messages_received = 0;
    size_t bytes_received = 0;
    while(true)
    {
        receiver.recv(&message);
        messages_received++;
        bytes_received += message.size();
        //std::cout << messages_received << " messages   - " << bytes_received << " Byte  - " << bytes_received/1024/1024 << " MB" << std::endl;
    }
    return 0;
}
