#include <iostream>
#include <string>
#include <chrono>
#include <unistd.h>

#include "netio/netio.hpp"


const char* HELP =
    R"FOO(Usage: netio_throughput [options]

Connets to a remote host and transfers messages while measuring the
achieved throughput.

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port on the remote. Default: 12345.
  -m BYTES        The size of a message that is transferred in bytes.
                  Default: 1024.
  -s MEGABYTES    The total amount of megabytes that is transferred.
                  Default: 1024 megabyte (1 GB).
  -c              Continuous mode.
  -t THREADS      The number of threads to use. Default: 1.
  -B PAGESIZE     Pagesize in byte. Default: 1 MB.
  -S SOCKETS      The number of sockets per thread. Default: 1.
  -P TAG          Use a subscribe socket with TAGS tags instead of a point-to-point socket.
  -k KHZ          Limit rate to the specified frequency in kilohertz.
  -h              Display this help message.
)FOO";


typedef std::chrono::high_resolution_clock stdclock;


typedef std::uint64_t timestamp_t;

static const timestamp_t ns = 1;
static const timestamp_t us = 1000 * ns;
static const timestamp_t ms = 1000 * us;
static const timestamp_t s = 1000 * ms;

timestamp_t gettime()
{
	::timespec ts;
	::clock_gettime(CLOCK_MONOTONIC, &ts);
	return timestamp_t(ts.tv_sec)*s + timestamp_t(ts.tv_nsec)*ns;
}


static void
run(netio::buffered_send_socket& socket, netio::message& msg, unsigned long long n_messages)
{
	for(unsigned long long i=0; i<n_messages; i++)
	{
		socket.send(msg);
	}

	socket.flush();
}


static void
run_with_rate_limitation(netio::buffered_send_socket& socket, netio::message& msg, unsigned long long n_messages,
	unsigned long long kilohertz)
{
	timestamp_t max_overshoot = 10 * ms;
	timestamp_t period = (1000./kilohertz) * us;

	timestamp_t now = gettime();
	timestamp_t deadline = now+period;

	for(unsigned long long i=0; i<n_messages; i++)
	{
		socket.send(msg);

		now = gettime();
		if(now > deadline + max_overshoot)
		{
			deadline = now + period;
		} else {
			while(now < deadline) {
				now = gettime();
			}
			deadline += period;
		}
	}
	socket.flush();
}


static void
run_subscribe(netio::context& ctx, const char* host, unsigned short port, netio::tag tag, size_t total_size)
{
	netio::subscribe_socket socket(&ctx);
	socket.subscribe(tag, netio::endpoint(host, port));

	size_t bytes_received = 0;
	while(bytes_received < total_size)
	{
		netio::message m;
		socket.recv(m);
		bytes_received += m.size();
	}
}



static void
print_statistics(stdclock::time_point t0, stdclock::time_point t1, unsigned n_threads, unsigned long long n_messages,
                 size_t msgsize, std::string host, unsigned short port)
{
	double seconds =  std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000000.;
	double GBps = (double)(n_threads*n_messages*msgsize)/(1024*1024*1024)/seconds;
	double Gbps = GBps*8;
	std::cout << "Remote host         : " << host << ":" << port << std::endl;
	std::cout << "Message size [Byte] : " << msgsize << std::endl;
	std::cout << "Number of messages  : " << n_messages*n_threads << std::endl;
	std::cout << "Total transfer [GB] : " << (n_threads*n_messages*msgsize)/(1024.*1024*1024) << std::endl;
	std::cout << "Rate [kHz]          : " << n_messages/seconds/1000 << std::endl;
	std::cout << "Time [s]            : " << seconds << std::endl;
	std::cout << "Throughput [GB/s]   : " << GBps << std::endl;
	std::cout << "Throughput [Gb/s]   : " << Gbps << std::endl;
}




int
main(int argc, char** argv)
{
	size_t msgsize = 1024;
	size_t transfer_bytes = 1024*1024*1024; // 1 GB
	unsigned short port = 12345;
	std::string host = "127.0.0.1";
	std::string backend = "posix";
	unsigned n_threads = 1;
	netio::tag tag = 0;
	bool use_subscribe_socket = false;
//	unsigned n_sockets = 1;
	bool continuous = false;
	unsigned long long kilohertz = 0;
	size_t pagesize = 1024*1024;

	char opt;
	while ((opt = getopt(argc, argv, "b:H:p:m:s:B:t:S:P:k:hc")) != -1)
	{
		switch (opt)
		{
		case 'b':
			backend = optarg;
			break;
		case 'c':
		  continuous = true;
		  break;
		case 'H':
			host = optarg;
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'm':
			msgsize = atoi(optarg);
			break;
		case 's':
			transfer_bytes = std::stoull(optarg)*1024*1024;
			break;
		case 'B':
			pagesize = std::stoull(optarg);
			break;
		case 't':
			n_threads = atoi(optarg);
			break;
		case 'P':
			use_subscribe_socket = true;
			tag = atoi(optarg);
			break;
		case 'k':
			kilohertz = atoi(optarg);
			break;
		case 'S':
//			n_sockets = atoi(optarg);
//      break;
      std::cerr << "-S not implemented yet" << std::endl;
      return -1;
		case 'h':
		default:
			std::cerr << HELP << std::endl;
			return -1;
		}
	}

	unsigned long long n_messages = transfer_bytes/msgsize;

	netio::context ctx(backend.c_str());

	std::thread bg_thread([&ctx](){
		ctx.event_loop()->run_forever();
	});



	netio::buffered_send_socket socket(&ctx, netio::sockcfg::cfg()
					   (netio::sockcfg::PAGESIZE, pagesize));
        socket.connect(netio::endpoint(host, port));
        while(!socket.is_open())
	  usleep(10);

	char* data = new char[msgsize];
	for(unsigned int i=0; i<msgsize; i++)
	{
		data[i] = 'x';
	}
	netio::message msg((uint8_t*)data, msgsize);

	// warmup run
	run(socket, msg, 1024);

	auto t0 = stdclock::now();

	do {
		if(use_subscribe_socket) {
			run_subscribe(ctx, host.c_str(), port, tag, transfer_bytes);
		} else {
			if(kilohertz > 0)
				run_with_rate_limitation(socket, msg, n_messages, kilohertz);
			else
				run(socket, msg, n_messages);
		}

		if(continuous) {
			auto t1 = stdclock::now();
			print_statistics(t0, t1, n_threads, n_messages, msgsize, host, port);
			t0 = t1;
		}

	} while(continuous);


	socket.disconnect();


	auto t1 = stdclock::now();




	print_statistics(t0, t1, n_threads, n_messages, msgsize, host, port);

	ctx.event_loop()->stop();
	bg_thread.join();

}
