#ifndef MEMPOOL_HPP
#define MEMPOOL_HPP
#include<cstddef>

template<typename T, unsigned int N>
class mempool
{
private :
    struct node              // linked list node
    {
        node * next;           // a reference to the next node
        T data[N];             // the data stored in the node
    };

private :
    node * memo;             // variable which points to the first node of the list
    node smallbuf;           // the memory itself, here will the objects be stored
    unsigned int available;  // number of memory chunks available

private :
    void push(node* nn)
    {
        // make next of new node as head
        nn->next = memo;

        // move the head to point to the new node
        memo = nn;
    }

    // recursive function to delete the entire
    // linked list
    void del(node* h)
    {
        if(h == &smallbuf)
            return ;

        del( h->next );

        delete h;
    }

public :
    mempool()
        : memo(0)
    {
        // set next pointer to null on smallbuf
        smallbuf.next = 0;

        // insert smallbuf at the top
        push(&smallbuf);

        // number of available memory chunks is the size of
        // the memory pool, naturally
        available = N;
    }

    ~mempool() { /* destructor of this class */ };

    T* allocate()
    {
        // allocation not possible if the memory pool
        // has no more space
        if(available <= 0)
        {
            // if memory pool cannot provide a proper memory node,
            // it will call the new function instead
            push(new node);

            available = N;
        }

        // number of free chunks decreased
        --available;

        // return a pointer to the allocated space
        return &memo->data[available];
    }

    inline
    void deallocate(...) { /* do nothing */ }

    void reset()
    {
        if(memo->next)
        {
            // delete linked list
            del(memo);
            memo = 0;

            push(&smallbuf);
        }

        available = N;
    }

    // return the number of bytes that are currently in use
    size_t inUseMemory()
    {
        size_t count = 0;

        for(node* n = memo; n; n=n->next, ++count)
        { }

        return (count-1)*N*sizeof(T) + (N-available)*sizeof(T);
    }
};

#endif//MEMPOOL_HPP
