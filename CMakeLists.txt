cmake_minimum_required(VERSION 3.4.3)
cmake_policy(SET CMP0074 NEW)

set(PACKAGE netio)
set(PACKAGE_VERSION 0.8.0)

include(FELIX OPTIONAL RESULT_VARIABLE FELIX_BUILD)

if(NOT FELIX_BUILD)
  tdaq_package()
endif()

set(VERSION "0.8.0")

## BINARY DISTRIBUTION USING CPACK ##

set(CPACK_RPM_PACKAGE_AUTOREQPROV " no")
set(CPACK_PACKAGE_VERSION ${VERSION})
set(CPACK_GENERATOR "RPM" "TGZ")
set(CPACK_PACKAGE_NAME "netio")
set(CPACK_PACKAGE_RELEASE 1)
set(CPACK_PACKAGE_CONTACT "Jörn Schumacher")
set(CPACK_PACKAGE_VENDOR "CERN")
#set(CPACK_PACKAGING_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}.${CMAKE_SYSTEM_PROCESSOR}")
include(CPack)


## Options ##

option(ENABLE_FIVERBS "enable the fi/verbs backend" ON)
set(VERSION_STRING ${VERSION})
configure_file(src/config.h.in config.h NEWLINE_STYLE UNIX)


## SOURCE DISTRIBUTION USING git-archive ##

set(SOURCE_DIST "${CPACK_PACKAGE_NAME}-${VERSION}")
add_custom_command(OUTPUT ${SOURCE_DIST}.tar.gz
                   COMMAND cd ${PROJECT_SOURCE_DIR} && git archive HEAD --format=tar  --prefix=${SOURCE_DIST}/ | gzip > ${CMAKE_BINARY_DIR}/${SOURCE_DIST}.tar.gz)
add_custom_target(dist DEPENDS ${SOURCE_DIST}.tar.gz)


## Check Dependencies ##
if(FELIX_BUILD)
  felix_add_external(libfabric ${libfabric_version} ${BINARY_TAG})
  # felix_add_external(zeromq ${zeromq_version} ${BINARY_TAG})
  # No FindZeroMQ.cmake available yet
  # find_package(ZeroMQ ${zeromq_version} EXACT REQUIRED)
  include_directories(${ZEROMQ_DIR}/include)
  link_directories(${ZEROMQ_DIR}/lib)
  include_directories(${LIBSODIUM_DIR}/include)
  link_directories(${LIBSODIUM_DIR}/lib)
endif()


## COMPILER SETUP ##

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -std=c++11 -Wall -g")
add_definitions(-std=c++11 -O2 -Wall -g)
add_definitions(-Wno-class-memaccess) # to make tbb warning disappear
add_definitions(-Wno-unused-variable) # to make test warning disappear
add_definitions(-Wno-pedantic) # to get rid of fiverbs warning
add_definitions(-Wno-format-truncation)
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.8)
    message(FATAL_ERROR "GCC version must be at least 4.8!")
  endif()
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -qopt-prefetch -unroll-aggressive -march=native -mtune=native")
endif()


## SOURCES AND TARGETS ##

include_directories(${CMAKE_BINARY_DIR})

if(NOT FELIX_BUILD)

tdaq_add_library(netio
  src/netio.cpp src/serialization.cpp src/posix.cpp  src/sockcfg.cpp src/endpoint.cpp
  src/message.cpp src/eventloop.cpp src/backend.cpp src/sockets.cpp src/context.cpp src/buffer.cpp
  LINK_LIBRARIES TBB rt
)

if(ENABLE_FIVERBS)
  target_sources(netio PRIVATE src/fi_verbs.cpp)
  target_link_libraries(netio PUBLIC fabric)
endif()

else()

find_package(TBB ${tbb_version} EXACT REQUIRED)
include_directories(${TBB_INCLUDE_DIRS})
link_directories(${TBB_DIR}/lib)

set(NETIO_SOURCES src/netio.cpp src/serialization.cpp src/posix.cpp  src/sockcfg.cpp src/endpoint.cpp
	src/message.cpp src/eventloop.cpp src/backend.cpp src/sockets.cpp src/context.cpp src/buffer.cpp)
if(ENABLE_FIVERBS)
  list(APPEND NETIO_SOURCES src/fi_verbs.cpp)
endif()

tdaq_add_library(netio SHARED ${NETIO_SOURCES})
target_link_libraries(netio rt tbb)
if(ENABLE_FIVERBS)
    target_link_libraries(netio fabric)
endif()

endif(NOT FELIX_BUILD)

set(NETIO_TEST_SOURCES test/test_main.cpp test/test_backend.cpp  test/test_buffer.cpp
    test/test_event_loop.cpp test/test_message.cpp  test/test_serialization.cpp test/test_sockets.cpp )
if(ENABLE_FIVERBS)
    list(APPEND NETIO_TEST_SOURCES test/test_fi_verbs.cpp test/test_sockets_fi_verbs.cpp)
endif()
tdaq_add_executable(netio_test ${NETIO_TEST_SOURCES})
target_link_libraries(netio_test netio rt pthread)

add_test(netio-test-buffer ./netio_test [buffer] -d yes)
add_test(netio-test-eventloop ./netio_test [eventloop] -d yes)
add_test(netio-test-message ./netio_test [message] -d yes)
add_test(netio-test-posix ./netio_test [posix] -d yes)
add_test(netio-test-serialization ./netio_test [serialization] -d yes)
add_test(netio-test-sockets ./netio_test [sockets] -d yes)
# add_test(netio-test-sockets_fi_verbs ./netio_test [sockets_fi_verbs] -d yes)

tdaq_add_executable(netio_cat src/main_cat.cpp)
target_link_libraries(netio_cat netio rt pthread)

tdaq_add_executable(netio_throughput src/main_throughput.cpp)
target_link_libraries(netio_throughput netio rt pthread)

tdaq_add_executable(netio_bw src/main_bw.cpp)
target_link_libraries(netio_bw netio rt pthread)

tdaq_add_executable(netio_pingpong src/main_pingpong.cpp)
target_link_libraries(netio_pingpong netio rt pthread)

tdaq_add_executable(netio_data_pub src/main_data_pub.cpp)
target_link_libraries(netio_data_pub netio rt pthread)

tdaq_add_executable(netio_recv src/main_recv.cpp)
target_link_libraries(netio_recv netio rt pthread)

if(FELIX_BUILD)
  tdaq_add_executable(netio_zeromq src/main_zeromq.cpp)
  target_link_libraries(netio_zeromq rt pthread zmq sodium)

  add_executable(netio_zeromq_pingpong src/main_zeromq_pingpong.cpp)
  target_link_libraries(netio_zeromq_pingpong rt pthread zmq sodium)

  add_executable(netio_zeromq_recv src/main_zeromq_recv.cpp)
  target_link_libraries(netio_zeromq_recv rt pthread zmq sodium)
endif()




## INSTALLATION ##
if(FELIX_BUILD)
install(TARGETS netio netio_recv netio_throughput netio_cat
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib)
install(FILES netio/netio.hpp DESTINATION include)
install(TARGETS netio netio_recv netio_throughput netio_cat EXPORT felix DESTINATION bin)
endif()
