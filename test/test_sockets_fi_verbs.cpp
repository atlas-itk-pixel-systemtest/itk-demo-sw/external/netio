#include "catch/catch.hpp"

#include "netio/netio.hpp"
#include "util.h"

using namespace netio;



TEST_CASE( "create a low-latency send socket (using fi_verbs)", "[sockets_fi_verbs]" )
{
    context ctx("fi_verbs");
    low_latency_send_socket socket(&ctx);

    REQUIRE( true );
}


TEST_CASE( "send and receive from a low-latency socket (using fi_verbs)", "[sockets_fi_verbs][!hide]")
{
    int port = get_free_port();

    context ctx("fi_verbs");
    low_latency_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    send.connect(endpoint(test_iface_addr(), port));
    while(!send.is_open())
        ctx.event_loop()->run_for(1000);
    send.send(message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == 14 );
}


TEST_CASE( "send and receive a large message from a low-latency socket (using fi_verbs)",
           "[sockets_fi_verbs][!hide]")
{
    int port = get_free_port();

    context ctx("fi_verbs");
    low_latency_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    const size_t SIZE = 15000;
    char* d = new char[SIZE];

    send.connect(endpoint(test_iface_addr(), port));
    while(!send.is_open())
        ctx.event_loop()->run_for(1000);
    send.send(message((uint8_t*)d, SIZE));
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == SIZE );
}


TEST_CASE( "create a buffered send socket (using fi_verbs)", "[sockets_fi_verbs]" )
{
    context ctx("fi_verbs");
    buffered_send_socket socket(&ctx);

    REQUIRE( true );
}


TEST_CASE( "send and receive from a buffered socket (using fi_verbs)", "[sockets_fi_verbs][!hide]")
{
    int port = get_free_port();

    context ctx("fi_verbs");
    buffered_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    send.connect(endpoint(test_iface_addr(), port));
    while(!send.is_open())
        ctx.event_loop()->run_for(1000);
    send.send(message((uint8_t*)"hello, world!", 14));
    send.flush();
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == 14 );
}


TEST_CASE( "send and receive from a buffered socket without a flush (using fi_verbs)",
           "[sockets_fi_verbs][!hide]")
{
    int port = get_free_port();

    context ctx("fi_verbs");
    buffered_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    send.connect(endpoint(test_iface_addr(), port));
    while(!send.is_open())
        ctx.event_loop()->run_for(1000);
    send.send(message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(4000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == 14 );
}


TEST_CASE( "send and receive large message from a buffered socket (using fi_verbs)",
           "[sockets_fi_verbs][!hide]" )
{
    int port = get_free_port();

    context ctx("fi_verbs");
    buffered_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    const size_t SIZE = 15000;
    char* d = new char[SIZE];
    REQUIRE( d != NULL );
    for(unsigned i=0; i<SIZE; i++)
    {
        d[i] = (2*i + 13) % 23;
    }
    message m((uint8_t*)d, SIZE);
    CAPTURE(m.size());

    send.connect(endpoint(test_iface_addr(), port));
    while(!send.is_open())
        ctx.event_loop()->run_for(1000);
    send.send(m);
    send.flush();
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == SIZE );
    for(unsigned i=0; i<SIZE; i++)
    {
        REQUIRE( d[i] == received[i] );
    }
    delete[] d;
}


TEST_CASE( "connect to a publish socket (using fi_verbs)", "[sockets_fi_verbs][!hide]" )
{
    int port = get_free_port();

    context ctx("fi_verbs");
    std::thread bg_thread([&ctx]()
    {
        ctx.event_loop()->run_forever();
    });

    publish_socket pub(&ctx, port);
    subscribe_socket sub(&ctx);

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    sub.subscribe(0, endpoint(test_iface_addr(), port));

    sleep(2);

    REQUIRE( subscribed == true );

    ctx.event_loop()->stop();
    bg_thread.join();
}


TEST_CASE( "publish with a publish socket (using fi_verbs)", "[sockets_fi_verbs][!hide]" )
{
    int port = get_free_port();

    context ctx("fi_verbs");
    std::thread bg_thread([&ctx]()
    {
        try
        {
            ctx.event_loop()->run_forever();
        }
        catch(std::exception& e)
        {
            INFO("exception in event loop: " << e.what());
        }
    });

    publish_socket pub(&ctx, port);
    subscribe_socket sub(&ctx);

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    sub.subscribe(5, endpoint(test_iface_addr(), port));
    sleep(2);
    pub.publish(5,  message((uint8_t*)"hello, world!", 14));
    sleep(1);

    message received;
    sub.recv(received);

    REQUIRE( subscribed == true );
    REQUIRE( received.size() == 14 );

    ctx.event_loop()->stop();
    bg_thread.join();
}


TEST_CASE( "subscribe to many tags (using fi_verbs)", "[sockets_fi_verbs][!hide]" )
{
    int port = get_free_port();

    const unsigned N = 10;

    context ctx("fi_verbs");
    std::thread bg_thread([&ctx]()
    {
        try
        {
            ctx.event_loop()->run_forever();
        }
        catch(std::exception& e)
        {
            INFO("exception in event loop: " << e.what());
        }
    });

    publish_socket pub(&ctx, port);
    subscribe_socket sub(&ctx);

    unsigned subscribed = 0;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed++;
    });

    unsigned unsubscribed = 0;
    pub.register_unsubscribe_callback([&](tag, endpoint)
    {
        unsubscribed++;
    });

    for(unsigned i=0; i<N; i++)
    {
      sub.subscribe(i, endpoint(test_iface_addr(), port));
    }
    sleep(2);
    REQUIRE( subscribed == N );

    for(unsigned i=0; i<N; i++)
    {
      sub.unsubscribe(i, endpoint(test_iface_addr(), port));
    }
    sleep(2);
    REQUIRE( unsubscribed == N );

    ctx.event_loop()->stop();
    bg_thread.join();
}
